#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  7 16:34:52 2019
by Pouye Yazdi
pouye.yazdi@upm.es
"""
import os
import sys
import numpy as np
from scipy.interpolate import griddata
import matplotlib
import matplotlib.pyplot as plt 
from matplotlib.colors import LogNorm
from mpl_toolkits.basemap import Basemap
import g

#### =======================================================
#### =======================================================
#### =======================================================
cmap = plt.cm.cubehelix_r
cmaplist = [cmap(i) for i in range(256)]
for i in range(25):
    cmaplist[i] = (1,1,1,1)
cmap = cmap.from_list('Custom cmap', cmaplist, cmap.N)
#### =======================================================
#### =======================================================
#### =======================================================    
def PlotKernelRates(plot_filename,plot_filepath,minMAG,maxMAG,title):

    r=open(os.path.join(plot_filepath,plot_filename),'r')    
    l=r.readlines()
    r.close()
    header=l[0]
    footer=l[-1]
    Mmin=round(float(header.split()[2]),1)
    Mmax=round(float(header.split()[-1]),1)
    print('The given Kernel rate file is for magnitudes between %.1f and %.1f, both included'%(Mmin,Mmax))
    if float(minMAG)<Mmin:
        print('WARNING! the given minimumMAG is less than mMin=%.1f in the kernel file!!!'%Mmin)
    if float(maxMAG)>Mmax:
        print('WARNING! the given maximumMAG is bigger than mMax=%.1f in the kernel file!!!'%Mmax)
    magnitude=np.arange(Mmin,Mmax+0.01,0.1)
    l=l[1:-1]
    lon=[]
    lat=[]
    magrate=np.zeros((len(l),len(magnitude)))
    for i in range(len(l)):
        lat=lat+[float(l[i].split()[0])]
        lon=lon+[float(l[i].split()[1])] 
        r=[]
        for m in range(len(magnitude)):
            r.append(round(float(l[i].split()[2+m]),9))  
        magrate[i]=r
    
    # =========================================================================
    n=int(np.sqrt((maxMAG-minMAG)/0.1))
    fig= plt.figure()
    smIndex=0.2
    incx,incy=(np.array(g.P(g.aproxlong+smIndex,g.aproxlat+smIndex))-np.array(g.P(g.aproxlong,g.aproxlat)))/1000
    incA=round(abs(incx*incy),1)  
    dA=round(float(footer.split()[0][14:-1]),2)
    loni=np.arange(min(lon)-smIndex,max(lon)+smIndex,smIndex)
    lati=np.arange(min(lat)-smIndex,max(lat)+smIndex,smIndex) 
    LON,LAT=np.meshgrid(loni,lati)
    totallanda=0
    k=0
    for j in range(len(magnitude)-1):
        if magnitude[j]>=minMAG-0.01 and magnitude[j]<maxMAG+0.01:
            k=k+1
            rate=[]    
            for i in range(len(l)):
                rate=rate+[magrate[i][j]]
            # rate is in area of size dA
            RATE=griddata((lon,lat),rate,(LON,LAT),method='linear')
            totallanda=totallanda+np.nansum(rate/(dA/incA))
            ax = fig.add_subplot(n+1,n+1,k)
            ax.set_title('$\int\lambda$=%0.1E'%(np.nansum(RATE/(dA/incA))),fontsize=6)
            lvls = np.logspace(-10,-3,8) 
            newlvls=[]
            for i in range(len(lvls)):
                newlvls.append(lvls[i])
                newlvls.append(lvls[i]*5) 
            newlvls=newlvls[1:-1]
            ax.contourf(LON,LAT,RATE/dA,levels=newlvls,cmap=cmap,norm = LogNorm())
            plt.text(min(lon), min(lat), '%.1f'%magnitude[j], fontsize=8,fontdict=None)
############ IF YOU WANT TO PLOT COASTLINES:
            map = Basemap(projection='merc', ellps = 'WGS84',resolution='i',epsg='4326',
                          llcrnrlat=min(lat),
                          urcrnrlat=max(lat),
                          llcrnrlon=min(lon),
                          urcrnrlon=max(lon))            
            map.drawcoastlines()
            map.drawmeridians([min(lon),max(lon)],labels=[False,False,False,True],fontsize=6)
            map.drawparallels([min(lat),max(lat)],labels=[True,False,False,False],fontsize=6)
############ IF YOU DO NOT NEED TO PLOT COASTLINES:
#            ax.set_xlim([min(lon),max(lon)])
#            ax.set_ylim([min(lat),max(lat)])
#            for tick in ax.get_xticklabels():
#               tick.set_fontsize(6)
#            for tick in ax.get_yticklabels():
#                tick.set_fontsize(6)
        
    ax1= fig.add_axes([0.25, 0.05, 0.5, 0.015])
    matplotlib.colorbar.ColorbarBase(ax1,cmap=cmap,boundaries=newlvls,norm = LogNorm(),
                            ticks=np.logspace(-9,-4,6),
                            orientation='horizontal',extend='both')
    for tick in ax1.get_xticklabels():
        tick.set_fontsize(8)
    ax1.set_title(label='$\lambda$ ($km^{-2}$)',fontsize=10)
    fig.suptitle('%s'%title,fontsize=12)
    plt.subplots_adjust(bottom=0.15, right=0.95, left=0.05,top=0.9, hspace=1.5, wspace= 0.7)
    plt.savefig(os.path.join(plot_filepath,'%s_Mag%.1fTo%.1f.png'%(plot_filename[:-4],minMAG,maxMAG)), dpi=300, facecolor='w', edgecolor='w',
        orientation='portrait',format='png') 
    plt.show()
#### =======================================================
#### =======================================================
#### =======================================================
def main(argv): 
    print ('***** This code is for plotting the density rates for given magnitude range.')
    Title=' '.join(argv[3:])
    PlotKernelRates(argv[0],g.ratefilepath,float(argv[1]),float(argv[2]),Title)


if __name__=='__main__':  
    ins=sys.argv[1:]
    if len(ins)<4:
        print('ERROR! Not enough inputs\n please enter inputs correctly with space between.\n Example:\n Kernel-filename 4.0 6.0 Figure Title')
        sys.exit(1)
    main(ins)
