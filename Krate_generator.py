#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 14 12:10:35 2018
by Pouye Yazdi
pouye.yazdi@upm.es
"""

"""
This code finds the rate of each mag increments for the calc grid
"""
import os
import sys
import numpy as np
import inputs
import g
import datetime
from math import exp,pi,cos,radians
from scipy.stats import norm
from compassbearing import Calculate_Bearing

#### =======================================================
#### =======================================================
#### =======================================================
def Catalog(catlines,mag,epsilon):
    newcat=[]
    i=0
    for line in catlines:
        if line.split():
            i=i+1
            m=round(float(line.split()[6]),1)
            if m>mag-(epsilon+0.1)*0.1 and m<mag+(epsilon+0.1)*0.1:
                newcat.append(line)
    return newcat
#### =======================================================
#### =======================================================
#### =======================================================
def Kernel(r2,H,theta,w_theta):
    kr=(2*(inputs.Q-1)/pow(H,2))*(1/pow((1+r2/pow(H,2)),inputs.Q))
    ktheta=(1/(2*pi+pi*w_theta))*(1+w_theta*pow(cos(theta),2))
    pdf=ktheta*kr
    return pdf
#### =======================================================
#### =======================================================
#### =======================================================
def Landa_prime(magcatalog,mag,sitelong,sitelat,sitedeltalong,sitedeltalat):
    sitex,sitey=g.P(sitelong,sitelat)#in m
    deltax,deltay=(np.array(g.P(sitelong+sitedeltalong,sitelat+sitedeltalat))-np.array(g.P(sitelong,sitelat)))/1000
#    deltaA=[]
    DeltaA=abs(deltax*deltay)
#    deltaA.append(DeltaA)
    rate=0
    H=inputs.D*exp(inputs.Gamma*mag)
    if magcatalog:
        for event in magcatalog:
            yeari=int(event.split()[0])
            refyeari=int(float(event.split()[8]))
            if yeari>=refyeari:              
                lati=float(event.split()[3])
                longi=float(event.split()[4])
                nut=inputs.endYear-refyeari
            
                w_th=int(float(event.split()[9]))
                directivity=float(event.split()[10])
#                if mag>=6.0: w_th=0.2
#                if mag>=7.0: w_th=0.5
#                if mag>=8.0: w_th=1
                th=radians(90-directivity)#convert to angel in polar sysytem
                
                magi=float(event.split()[6])
                magError=float(event.split()[7])
                pdfm=norm.pdf(mag,magi,magError) 
                probm=pdfm*0.1
                xi,yi=g.P(longi,lati)#in m
                epierrxy=float(event.split()[5])
############################################################################### 
                if inputs.Epierrindex==1:
                    percentils=np.array([0])
                elif inputs.Epierrindex==3:
                    percentils=np.array([-0.968,0,0.968])
                elif inputs.Epierrindex==5:
                    percentils=[-1.28,-0.523,0,0.523,1.281]
                else:
                    percentils=np.array([0])
                xii=percentils*epierrxy+xi/1000
                yii=percentils*epierrxy+yi/1000
                probr=0
                for x in xii:
                    for y in yii:
                        long,lat=g.P(x,y,inverse=True)
                        az=Calculate_Bearing((lat,long),(sitelat,sitelong))
                        phi=radians(90-az) #convert to angel in polar sysytem
                        deltaPhi=phi-th
                        dist2=pow(sitey/1000-y,2)+pow(sitex/1000-x,2)
                        pdfr=Kernel(dist2,H,deltaPhi,w_th)  
                        probr=probr+pdfr*DeltaA
                probr=probr/(inputs.Epierrindex*inputs.Epierrindex)
###############################################################################                 
#                # new epicenter error implantation from here (it takes double time)
#                if Epierrindex==1:#no distribution
#                    percentils=np.array([0])
#                    probE=np.array([1])
#                elif Epierrindex==3:#distribution with sigma=epierrxy for -sigma to +sigma
#                    percentils=np.array([-1,0,1])
#                    probE=norm.pdf(percentils*epierrxy,0,epierrxy)*epierrxy
#                    ##probE.sum() is less than 1 : for epierrxy=7: it will be 0.882 
#                    probE=probE/sum(probE)
#                elif Epierrindex==5:#distribution with 2sigma=epierrxy for -2sigma to +2sigma
#                    percentils=np.array([-1,-0.5,0,0.5,1])
#                    probE=norm.pdf(percentils*epierrxy,0,epierrxy/2)*epierrxy/2
#                    ##probE.sum() is less than 1 : for epierrxy=7: it will be 0.990
#                    probE=probE/sum(probE)
#                else:
#                    np.array([0])
#                xii=percentils*epierrxy+xi/1000
#                yii=percentils*epierrxy+yi/1000
#                probr=0
#                for x in xii:
#                    probx=probE[[list(xii).index(x)]]
#                    for y in yii:
#                        proby=probE[[list(yii).index(y)]]
#                        long,lat=P(x,y,inverse=True)
#                        az=Calculate_Bearing((lat,long),(sitelat,sitelong))
#                        phi=radians(90-az) #convert to angel in polar sysytem
#                        deltaPhi=phi-th
#                        dist2=pow(sitey/1000-y,2)+pow(sitex/1000-x,2)
#                        pdfr=Kernel(dist2,H,deltaPhi,w_th)   
#                        probr=probr+pdfr*DeltaA*proby*probx
#                probr=probr[0] # to covert array to value
#               # up to here
###############################################################################                 
                probr=probr/nut
                rate=rate+(probr*probm) 
#    print('the mean value of dA is %.3f'%np.mean(deltaA))
    return rate
#### =======================================================
#### =======================================================
#### =======================================================
    
def KERNEL_RATES(calculationGrdid,catalog,magnitudeList,outputfile,LandaFunction):
    N=0
    outputfile.writelines('lat long %s\n'%(' '.join('%s'%m for m in magnitudeList)))
    for cg in calculationGrdid:
        N=N+1
        #### =======================================================
        #### The coordinates of the calculation site
        cglon,cglat=float(cg.split()[0]),float(cg.split()[1])
        #### in Km
        cgx,cgy=np.array(g.P(cglon,cglat))/1000   
        print('\n****\nFor Calc Point %.2f , %.2f'%(cglon,cglat))    
        #### =======================================================
        #### calculating the rate of all magnitude increment at each site 
        landa_cg=[] 
        for mi in magnitudeList:
#            print('--Magnitude: %.1f'%mi)
            magcat=Catalog(catalog,mi,inputs.Epsilon_m)
            #### =======================================================
            #### landa_m_cg=landa(mi) at site cg (in unit of space and time)
            landa_m_cg=LandaFunction(magcat,mi,cglon,cglat,inputs.dlong,inputs.dlat)
#            print('--The landa(M=%.1f) @ calc point is estimated'%mi)
            #### =======================================================
            #### landa_cg=[landa(m1),landa(m2),landa(m3),....] at site cg (in unit of space and time)
            landa_cg.append(round(landa_m_cg,8))
                        
        print('Lets write the result into output file (line %i)'% N)
        rate_cg=' '.join(str(float("{0:.2E}".format(land))) for land in landa_cg)
        outputfile.writelines('%7.2f %6.2f %s\n'%(cglat,cglon,rate_cg))
    outputfile.writelines('MeanOfdA(km2)=%.2f  UsedCatalog=%s_%s  EndYear=%.1f  EpsilonM=%i  Epierrindex=%i  D=%.3f  Q=%.1f  Gamma=%.2f\n'%(inputs.deltaA,
                          inputs.catfilename,inputs.suffix,inputs.endYear,inputs.Epsilon_m, inputs.Epierrindex, inputs.D, inputs.Q, inputs.Gamma))
    outputfile.close()
    return
#### =======================================================
#### =======================================================
#### =======================================================
#### =======================================================
""" 
***** IMPORTANT *****
The result of Landa or Landa_prime gives the annual number of events with 
associated magnitude increment in area increment of size [dlong(in km) x dlat(in km)].
That's why the summation on all the zone's points should give the total number of 
that magnitude increment in the zone.

However, for making a contour plot which contineusly colors the zone, it is more
logical if we use annual rate in unit of area (1 Km2)
So, the output of landa or landa_prime should be 
devided to [dlong(in km) x dlat(in km)]
"""
def main():
    Landaf=Landa_prime
    #### =======================================================
    print ('***** Python version: ******\n'+sys.version+'\n--------------------')
    print ('***** This code is for estimation of spatial rate for each magnitude increment.')   
    print('***** The below settings will be applied:')
    print('if they are not as you desire, change the setting by modifying inputs in inputs.py')
    print('\nmMin is %.1f\nmMax is %.1f'%(inputs.mMin, inputs.mMax))
    print('\nMagnitude error is given in the catalog\nNumber of +/-errors used for each magnitude is %i'%inputs.Epsilon_m)
    print('\nEach epicentre is considered to have %i possible location'%(inputs.Epierrindex*inputs.Epierrindex))
    print('\nKernel parameters are:\nd=%.3f\nq=%.3f\nGamma=%.3f'%(inputs.D,inputs.Q,inputs.Gamma))
    print('\nThe Kernel rates will be calculated on a grid file with:\n    Latitude increment=%.3f degree\n    Longitude increment=%.3f degree'%(inputs.dlat,inputs.dlong))
    print('This file is called %s inside path:\n %s'%(inputs.rate_calcgridfilename,g.inputsfilepath))
    print('\nFor the given geographical zone the approximate size of increments are:\n    Latitude increment=%.3f km\n    Longitude increment=%.3f km'%(inputs.dy,inputs.dx))
    print('    Approximate area increment=%.3f'%inputs.deltaA)
    print('\nThe applied catalog "%s" is inside folder:\n%s\nand it ends in %.1f'%(inputs.catfilename,g.inputsfilepath,inputs.endYear))
    print('\nThe Kernel rates file will be called:\n "%s" and will be put inside:\n %s'%(inputs.ratefilename,g.ratefilepath))
    print ('And the Kernel distribution by WOO will be used.')
    print ('***** By POUYE.YAZDI@UPM.ES ******\n\nSTART\n--------------------') 
    time=datetime.datetime.now() 
    calcgridfile=open(os.path.join(g.inputsfilepath,inputs.rate_calcgridfilename),'r')
    calcgrids=calcgridfile.readlines()
    calcgridfile.close()
    #### =======================================================
    catfile=open(os.path.join(g.inputsfilepath,inputs.catfilename),'r')
    cat=catfile.readlines()[1:]
    catfile.close()
    #### =======================================================
    magnitudeRange=np.arange(inputs.mMin,inputs.mMax+0.01,0.1)
    #### =======================================================
    ratefile=open(os.path.join(g.ratefilepath,inputs.ratefilename),'w')
    KERNEL_RATES(calcgrids,cat,magnitudeRange,ratefile,Landaf)
    #### =======================================================
    print('Elapsed time for KERNEL_RATES function : %s'%(datetime.datetime.now()-time))
if __name__=='__main__':  
    main()
