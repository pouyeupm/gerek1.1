#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec  1 21:41:37 2019
by Pouye Yazdi
pouye.yazdi@upm.es
"""

from mpl_toolkits.basemap import Basemap
from shapely.geometry import Polygon 
import g

appliedratefilename='Krates_all_min4.0_max8.4_Q2.0_G0.50_D0.700_Em1_EEI3_dA472.2km2_Guerrero1900-2018.txt'
# =======================================================
# +++  Geographycal limits for hazard estimation   ++++++
# =======================================================
minlath=17
maxlath=18
minlonh=-102
maxlonh=-100
#### =================================================
#### if you want to use a 3d geometry for the source then set constantDepth=0
#### otherwise the given geometry file will be discarded and a constant depth value will be applied
#### Therefor if you do not have a 3D geometry and want to use a constant depth for the source,then give it as a poistive value and in km
magnitude_dependent_depth_function=False
constantDepth=0
sourcegeometryfilename='subduction-grid-inc01-0.txt' # long lat +depth(km) 
#### if you want to use a magnitude dependent depth put magnitude_dependent_depth_function=True 
#### hence, constantDepth and sourcegeometryfilename won't be read
#### =================================================
haz_dlong=0.5
haz_dlat=0.5
Accelerationlist=[20,40,60,80,100,200,300,400,500,600,700,800,900,1000,1200,1400,1600,1800,2000] #in gal or cm/s2
mMinHazard=5.0
#### =================================================
#
#
#
#### End of inputs for GERE1y_generator.py
#### Do not make change in the following lines
#### Unless you want to :-D
#
#
#
#
#
#
#
#
#
#
#
#
#
#
map = Basemap(projection='merc', ellps = 'WGS84',# doesn't need to be mercator
                  llcrnrlat=minlath,
                  urcrnrlat=maxlath,
                  llcrnrlon=minlonh,
                  urcrnrlon=maxlonh,
                  resolution='i',
                  epsg='4326')
coastpolygon=map.coastpolygons[0] 
polypoint=[]
for i in range(len(coastpolygon[0])):
    polypoint.append((coastpolygon[0][i],coastpolygon[1][i]))
Landpoly=Polygon(polypoint)
rectanglepolypoints=[(minlonh,minlath),(minlonh,maxlath),(maxlonh,maxlath),(maxlonh,minlath)]
Recpoly=Polygon(rectanglepolypoints)
Seapoly=Recpoly.difference(Landpoly)

haz_calcgridfilename='HazardGrid%sx%s_Lat%.1f_%.1fLon%.1f_%.1f.txt'%(str(haz_dlong).replace(".",""),str(haz_dlat).replace(".",""),minlath,maxlath,minlonh,maxlonh) #long lat
g.makegridfile(minlonh,maxlonh,minlath,maxlath,haz_dlong,haz_dlat,haz_calcgridfilename)

appliedratefilepath=g.ratefilepath
gere1yfilename='GERE1yMmin%.1f_%s_%s.txt'%(mMinHazard,haz_calcgridfilename[:-4],appliedratefilename[:-4])
gere1yfilepath=g.hazfilepath



