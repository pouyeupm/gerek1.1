# GEREK1.1
Ground motion **Exceedance Rate Estimation** with **Kernel** distribution for earthquake densities.

This program consists of two main modules:
1) **Krate_generator.py**
2) **GERE1y_generator.py**

The first module creates earthquake density rates (<img src="https://latex.codecogs.com/gif.latex?\frac{1}{km^2y} "/>) for a given earthquake catalog. And the second module is for estimating ground motion exceedance one year.

First, you need to check **g.py** and modify it as you wish.
Pay attention that the directories you define here should already exist inside the main directory. If they do not, you need to manually create them.

For running **Krate_generator.py** please define the associated inputs in **inputs.py**. All your giving input values (not the geographical limits: lat and long) will automatically define the name of the Kernel rate file that is going to be created. These inputs will also be written at the bottom of the created file.

For running **GERE1y_generator.py** please define the associated inputs in **inputsH.py**. The given minimum magnitude, the name of the used grid file (has to be inside the *inputs directory*) and the name of the applied Kernel rate file will automatically define the name of the output of this module that gives the Ground motion Exceedance Rate Estimation for 1 year. 

The execution time for a dense grid file can be quite long!
