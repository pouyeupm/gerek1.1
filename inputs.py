#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  6 13:15:57 2018
by Pouye Yazdi
pouye.yazdi@upm.es
"""

import numpy as np
import g

# =======================================================
# +++++++++++ The applied earthquake catalog ++++++++++++
# =======================================================
catfilename='Guerrero1900-2018.txt'
suffix='all'
endYear=2018 #used for kernel_rate_generator
# =======================================================
# +++++  Kernel Rate Making inputs  ++++++++++++++++++++
# =======================================================
mMin=4.0   
mMax=8.4 #### For calculating Kernel Rates
#### ====================================================
####  MAGNITUDE UNCERTAINITY
Epsilon_m=1 # m inside m+Epsilon_m*(Sigma_m) and m-Epsilon_m*(Sigma_m)
#### ====================================================
#### EPICENTER UNCERTAINITY
Epierrindex=3
#### ====================================================
#### KERNEL PARAMS
D=0.7
Q=2.0
Gamma=0.5
# =======================================================
# +++  Geographycal limits for kernel rate estimation  ++
# =======================================================
minlatr=15
maxlatr=20
minlonr=-104
maxlonr=-98
#### ====================================================
#### dlong dlat are used for more accurate kernel rates estimation at each site
dlong=0.4
dlat=0.4

#### ====================================================
#
#
#
#### End of inputs for Krate_generator.py
#### Do not make change in the following lines
#### Unless you want to :-D
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#### an approximate estimation of area increment or area of each cell (in Km x Km)
dx,dy=(np.array(g.P(g.aproxlong+dlong,g.aproxlat+dlat))-np.array(g.P(g.aproxlong,g.aproxlat)))/1000
#### deltaA is used as a mean of area increment when we plot the kernel rate
#### so each color shows an estimation for the rate of earthquakes in unit of area.
deltaA=round(abs(dx*dy),1)
#### =================================================
#### building the grid file for kernel rate estimations
rate_calcgridfilename='kernelrateGrid%sx%s_Lat%.1f_%.1fLon%.1f_%.1f.txt'%(str(dlong).replace(".",""),str(dlat).replace(".",""),minlatr,maxlatr,minlonr,maxlonr) #long lat
g.makegridfile(minlonr,maxlonr,minlatr,maxlatr,dlong,dlat,rate_calcgridfilename)
# =======================================================
# ++++++++++ kernel rate file directoty and name ++++++++
# =======================================================
name='min%.1f_max%.1f_Q%.1f_G%.2f_D%.3f_Em%i_EEI%i'%(mMin,mMax,Q,Gamma,D,Epsilon_m,Epierrindex)
ratefilename='Krates_%s_%s_dA%.1fkm2_%s.txt'%(suffix,name,deltaA,catfilename[:-4])



