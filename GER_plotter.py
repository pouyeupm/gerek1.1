#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec  3 10:44:50 2019
by Pouye Yazdi
pouye.yazdi@upm.es
"""
import os
import sys
import numpy as np
from scipy.interpolate import griddata
import matplotlib
import matplotlib.pyplot as plt 
from mpl_toolkits.basemap import Basemap
from descartes import PolygonPatch
from scipy.interpolate import interp1d
from inputsH import Seapoly
import g

#### =======================================================
#### =======================================================
#### =======================================================
cmap = plt.cm.plasma_r
cmaplist = [cmap(i) for i in range(0,200)]
#    for i in range(10):
#        cmaplist[i] = (1,1,1,1)
cmap = matplotlib.colors.LinearSegmentedColormap.from_list('Custom cmap', cmaplist, cmap.N)
#### =======================================================
#### =======================================================
#### =======================================================
def PlotGERates(plot_filename,plot_filepath,Nyears,probabilityList):
    
    r=open(os.path.join(plot_filepath,plot_filename),'r')    
    l=r.readlines()
    r.close()
    header=l[0]
    acclist=[round(float(a),1) for a in header.split()[2:]]
    accMin=acclist[0]
    accMax=acclist[-1]
    print('The given Ground motion Exceedance Rate file is estimated for %i accelerations between %.1f and %.1f, both included'%(len(acclist),accMin,accMax))            
    l=l[1:]
    
    
    lonLIST=[]
    latLIST=[]
    exr1yLIST=[]
    pLIST=[]
    aLIST=[]
    for i in range(len(l)):
        lat=float(l[i].split()[0])
        lon=float(l[i].split()[1])
        exr1ylist=[round(float(a),9) for a in l[i].split()[2:]]
        exr1yArray=np.array(exr1ylist)
        pArray=1-np.exp(-1*exr1yArray*Nyears)
        plist=list(pArray)
        try:
            interpolation_pArray=interp1d(np.log10(plist),np.log10(acclist), kind='linear')
        except:
            print('--interpolation is not possible for point at %.2f,%.2f'%(lat,lon))
        lonLIST.append(lon)
        latLIST.append(lat)
        exr1yLIST.append(exr1ylist)
        pLIST.append(plist) 
        
        alist=[]
        for p in probabilityList:
            try:
                log10accel=interpolation_pArray(np.log10(p))
            except:
                log10accel=0
            if log10accel==0:
                alist=alist+[np.NaN]
            else:
                alist=alist+[round(float(np.power(10,log10accel)),2)]
        aLIST.append(alist)
    
    pLIST=np.reshape(pLIST,(len(l),len(acclist)))     
    aLIST=np.reshape(aLIST,(len(l),len(probabilityList)))   
# =========================================================================
    n=int(np.sqrt(len(probabilityList)))
    fig= plt.figure(2,figsize=(8,5))
    smIndex=0.2
    loni=np.arange(min(lonLIST),max(lonLIST)+smIndex,smIndex)
    lati=np.arange(min(latLIST),max(latLIST)+smIndex,smIndex) 
    LON,LAT=np.meshgrid(loni,lati)
    
    k=0
    for i in range(len(probabilityList)):
        A=griddata((lonLIST,latLIST),aLIST[:,i]/1000,(LON,LAT),method='linear')# in g
        print('For exceedance probability of %.5f in %i years, the maximum estimated acceleration is %f'%(probabilityList[i],Nyears,np.nanmax(A)))
        k=k+1
        ax = fig.add_subplot(n+1,n+1,k)
        ax.set_title('P=%.5f'%probabilityList[i],fontsize=8)
        lvls = np.linspace(0,1.1,12)
        ax.contourf(LON,LAT,A,levels=lvls,cmap=cmap,extend='max')
        CS=ax.contour(LON,LAT,A,levels=lvls,linewidths=0.2, colors='k')
        ax.clabel(CS, inline=1,fmt='%.1f', fontsize=8,zorder=3)     
        Seapatch = PolygonPatch(Seapoly, facecolor='lightblue', alpha=1.0, zorder=2)
        ax.add_patch(Seapatch)
############ IF YOU WANT TO PLOT COASTLINES: 
        map = Basemap(projection='merc', ellps = 'WGS84',resolution='i',epsg='4326',
                      llcrnrlat=min(latLIST),
                      urcrnrlat=max(latLIST),
                      llcrnrlon=min(lonLIST),
                      urcrnrlon=max(lonLIST))           
        map.drawcoastlines()
        map.drawmeridians([min(lonLIST),max(lonLIST)],labels=[False,False,False,True],fontsize=6)
        map.drawparallels([min(latLIST),max(latLIST)],labels=[True,False,False,False],fontsize=6)
############ IF YOU DO NOT NEED TO PLOT COASTLINES:
#        ax.set_xlim([min(lonLIST),max(lonLIST)])
#        ax.set_ylim([min(latLIST),max(latLIST)])
#        for tick in ax.get_xticklabels():
#            tick.set_fontsize(6)
#        for tick in ax.get_yticklabels():
#            tick.set_fontsize(6)

    ax1= fig.add_axes([0.6, 0.05, 0.3, 0.02])
    matplotlib.colorbar.ColorbarBase(ax1,cmap=cmap,boundaries=lvls,
                            ticks=np.linspace(0,1.0,11),
                            orientation='horizontal',extend='max')
    for tick in ax1.get_xticklabels():
        tick.set_fontsize(8)
    ax1.set_title(label='PGA (g)',fontsize=8)
    fig.suptitle('Estimated Ground Motion Level in %i Years'%Nyears,fontsize=8)
    plt.subplots_adjust(bottom=0.15, right=0.9, left=0.1,top=0.9, hspace=0.5, wspace= 0.5)
    plt.savefig(os.path.join(plot_filepath,'%s_In%iY.png'%(plot_filename[:-4],Nyears)), dpi=300, facecolor='w', edgecolor='w',
        orientation='portrait',format='png') 
    plt.show()

#### =======================================================
#### =======================================================
#### =======================================================
def main(argv): 
    print ('***** This code is for plotting the hazard maps for given probabilities.')
    PlotGERates(argv[0],g.hazfilepath,int(argv[1]),[float(a) for a in argv[2:]])

if __name__=='__main__':  
    ins=sys.argv[1:]
    if len(ins)<3:
        print('ERROR! Not enough inputs\n Please enter inputes with space between them.\n Example:\n GER1y-filename 1 100 400 1100')
        sys.exit(1)
    main(ins)
