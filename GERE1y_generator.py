#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec  5 11:59:49 2019
by Pouye Yazdi
pouye.yazdi@upm.es
"""
import os
import sys
import numpy as np
import datetime
import scipy
from scipy.stats import norm
from shapely.geometry import Point
import inputsH
import geopy.distance
# from geopy.distance import VincentyDistance #for older versions geopy
from geopy.distance import VincentyDistance
import math
from scipy.interpolate import griddata
import g

"""
This code uses the ratefile made by KERNEL_RATES function as the main input. 
Then it calculate the Ground motion Exceedance Rate (GER) at any calculation 
site.
For each magnitudes, it makes a different grid of nodes (for picking up the 
rate) around the calculation site ---> it uses hazardGrid function below.
The GER at each calculation site should be calculated using all 
nodes of the constructed grid.
The calculations site which are offshore will be discarded in the process.
It is important to have the depth of the grid points. If there is no depth
value for them they will be discarded in calculation of the GER.
"""
#### =======================================================
#### =======================================================
#### =======================================================
def rotate(X, theta):
    c, s = np.cos(np.radians(theta)), np.sin(np.radians(theta))
    return np.dot(np.array([
    [c, -s ],
    [s,  c ],
    ]),X)
    
def hazardGrid(m):
    if m<=4.5:
        radial_distance=[1,3,6,9,12,15,20,25]
        dr=[2,4.5,7.5,10.5,13.5,17.5,22.5,27]
    elif m<=5.0:
        radial_distance=[1,3,6,9,12,15,20,25,30,40]
        dr=[2,4.5,7.5,10.5,13.5,17.5,22.5,27.5,35,42]
    elif m<=5.5:
        radial_distance=[1,3,6,9,12,15,20,25,30,35,40,50]
        dr=[2,4.5,7.5,10.5,13.5,17.5,22.5,27.5,32.5,37.5,45,52]
    elif m<=6.0:
        radial_distance=[1,3,6,9,12,15,20,25,30,35,40,50,60,70,80]
        dr=[2,4.5,7.5,10.5,13.5,17.5,22.5,27.5,32.5,37.5,45,55,65,75,85]
    elif m<6.5:
        radial_distance=[1,3,6,9,12,15,20,25,30,35,40,45,55,65,80,100]
        dr=[2,4.5,7.5,10.5,13.5,17.5,22.5,27.5,32.5,37.5,42.5,50,60,72.5,90,110]
    else:
        radial_distance=[1,3,6,9,12,15,20,25,30,35,40,45,55,65,80,100,150,200]
        dr=[2,4.5,7.5,10.5,13.5,17.5,22.5,27.5,32.5,37.5,42.5,50,60,72.5,90,125,175,210]
    return radial_distance,dr,20
#### =======================================================
#### =======================================================
#### =======================================================
def Rates(minM):
    r=open(os.path.join(inputsH.appliedratefilepath,inputsH.appliedratefilename),'r')    
    l=r.readlines()
    r.close()
    header=l[0]
    footer=l[-1]
    minKr=round(float(header.split()[2]),1)
    maxKr=round(float(header.split()[-1]),1)
    
    l=l[1:-1]
    longR=[]
    latR=[]
    for i in range(len(l)):
        longR.append(float(l[i].split()[1]))
        latR.append(float(l[i].split()[0]))
        
    if minM<(minKr-0.01):
        print('The given min magnitude for GERE is lower than minimum magnitude for the Kernel rates\n It is now set to %.1f'%(round(float(header.split()[2],1))))
        minM=minKr
    magRange=np.arange(minM,maxKr+0.01,0.1) 
 
    density_rates=[]
    for mag in magRange:
        n=int((mag-minKr)*10)
        magrate=[]      
        for i in range(len(l)):
            magrate.append(round(float(l[i].split()[2+n]),10))
        density_rates.append(np.array(magrate)/round(float(footer.split()[0][14:-1]),2))
    return longR,latR,magRange,density_rates
#### =======================================================
#### =======================================================
#### =======================================================
def P_exc_rate_mexicansubduction(a,mag,r,z): 
    #this function gets the entered as z! that can be a constant value inputsH.constantDepth or can be the result of reading the 3Dsource geometry file
    prob=0
    if z>5:
        zdistribution=np.array([-5,0,5,15])
        wi=np.array([0.25,0.35,0.25,0.15])
        zrange=zdistribution+z
        for i in range(4):
            ln_a,Sigma_lna=Att(mag,r,zrange[i])
            Z=(ln_a-np.log(a))/Sigma_lna
            prob=prob+(norm.cdf(Z)*wi[i])                 
    return prob
#### =======================================================
#### =======================================================
#### =======================================================
def P_exc_rate(a,mag,r,z): 
    #this function gets the entered as z! that can be a constant value inputsH.constantDepth or can be the result of reading the 3Dsource geometry file
    prob=0
    if z>5:
        zdistribution=np.array([-5,0,5])
        wi=np.array([0.25,0.5,0.25])
        zrange=zdistribution+z
        for i in range(3):
            ln_a,Sigma_lna=Att(mag,r,zrange[i])
            Z=(ln_a-np.log(a))/Sigma_lna
            prob=prob+(norm.cdf(Z)*wi[i])                 
    return prob  
#### =======================================================
#### =======================================================
#### =======================================================
def P_exc_rate_depthDependent(a,mag,r,z):
    # This function does not care what you enter as z! the z is defined here
    # Define your depth dependency in the block below
    ########################################################
    if mag<5:
        z=10
    elif mag<6:
        z=15
    else:
        z=20
    sigma_z=5 #km
    ########################################################
    zdistribution=np.array([-2*sigma_z,-sigma_z,0,sigma_z,2*sigma_z])
    wi=norm.pdf(zdistribution/sigma_z)
    zrange=zdistribution+z
    prob=0
    for i in range(len(wi)):
        ln_a,Sigma_lna=Att(mag,r,zrange[i])
        Z=(ln_a-np.log(a))/Sigma_lna
        prob=prob+(norm.cdf(Z)*wi[i])                 
    return prob
#### =======================================================
#### =======================================================
#### =======================================================
#### GMPE by Arroyo et al. 2010
#def Att(mag,r,z):
#    R=np.sqrt(r*r+z*z)
#    r02=1.4447E-5 *np.exp(2.3026*mag)                                   
#    C=0.0150*R
#    D=0.0150*np.sqrt(R*R+r02)
#    CC=-1*scipy.special.expi(-C)
#    DD=-1*scipy.special.expi(-D)
#    A=(CC-DD)/(r02)
#    lna=2.4862+(0.9392*mag)+(0.5061*np.log(A))
#    sigmalna=0.75   
#    return lna,sigmalna 
#### =======================================================
#### =======================================================
#### =======================================================
#### GMPE by Akkar 2014
def Att(mag,r,z):
    R=np.sqrt(r*r+z*z)
    a1=3.26685
    a2=0.0029
    a3=-0.04846
    a4=-1.47905
    a5=0.2529
    a6=7.5
    a7=-0.5096
    c1=6.75
    S=0
    if mag<=c1:
        a=a2
    else:
        a=a7
    lna=+np.log(1000)+a1+(a*(mag-c1))+a3*np.power(8.5-mag,2)+(a4+a5*(mag-c1))*np.log(np.sqrt(R*R+a6*a6))+S
    sigmalna=0.7347   
    return lna,sigmalna   
#### =======================================================
#### =======================================================
#### =======================================================

def GERE_HAZARD(calculationGrdid,sourceGeometry,minHazardMag,outputfile,PexceedaFunction):
    print('(V) GERE_HAZARD is going to be executed for\n %i asked strong ground motion values: %s'%(len(inputsH.Accelerationlist),inputsH.Accelerationlist))
    print('(VI) The magnitude density rates are picked up from the result of KERNEL_RATES function.')
    #### rates are in 1Km2
    lonr,latr,magrange,rates=Rates(minHazardMag)

    glon=[]
    glat=[]
    gz=[]
    if type(sourceGeometry)==list:
        print('(VII) You have introduced a 3D geometry that is used for calculation of source depth.')
        for grid in sourceGeometry:
            gridlon,gridlat=float(grid.split()[0]),float(grid.split()[1])
            gridz=round(float(grid.split()[2]),2)
            glon.append(gridlon) # in km
            glat.append(gridlat) # in km
            gz.append(gridz) # in km and positive
    elif type(sourceGeometry)==np.ufunc:
        print('(VII) You are using a magnitude dependent depth function.')
        sourceGeometry=1
    else:
        print('(VII) You have introduced a constant depth of %.f km for your source.'%sourceGeometry)
             
    outputfile.writelines('lat long %s\n'%(' '.join('%s'%a for a in inputsH.Accelerationlist)))
    N=0
    inlandN=0
    for cg in calculationGrdid:
        N=N+1
        #### =======================================================
        #### Converting lat long coordinates of the site to km 
        cglon,cglat=float(cg.split()[0]),float(cg.split()[1]) 
        cgx,cgy=np.array(g.P(cglon,cglat))/1000 
        print('\n********\n--%i-For calculation site %.2f , %.2f'%(N,cglon,cglat))    
        #### =======================================================
        #### The exceedance rate of strong ground motion because for all asked acceleration values
        exceedance_rate=np.arange(len(inputsH.Accelerationlist))*0         
#        if not inputsH.Landpoly.contains(Point(cglon,cglat)):
#            print('*--This calculation point is outside land and will be skipped')
#        else:
        inlandN=inlandN+1  
        for h in range(len(magrange)):
            mi=magrange[h]
            rr=rates[h]
            print('----Magnitude: %.1f'%mi) 
            rdist,dr,delta_theta=hazardGrid(mi)
            a=[np.pi*dr[0]*dr[0]/(360/delta_theta)]+[np.pi*(dr[i]*dr[i]-dr[i-1]*dr[i-1])/(360/delta_theta) for i in range(1,len(dr))]
            origin = geopy.Point(cglat,cglon)
            lat_long_dA=[]
            i=-1
            for dd in rdist:
                i=i+1
                for b in np.arange(10,355,20): 
                    # destination = VincentyDistance(kilometers=dd).destination(origin, b)  # for older versions of geopy
                    destination = VincentyDistance(kilometers=dd).destination(origin, b)
                    lat, lon = destination.latitude, destination.longitude
                    lat_long_dA.append([lat,lon,a[i]])
            LATLONGA=np.matrix(lat_long_dA)
            print('----A circular grid of radius %ikm containing %i points is made'%(max(rdist),len(lat_long_dA))) 
            LAT=LATLONGA[:,0]
            LONG=LATLONGA[:,1]
            DA=LATLONGA[:,2]
                
            #### glon,glat,gz are the read from the file that contains source geometry coordinates
            #### depth of the grid point is:
            if not len(gz)==0:
                Z=np.matrix(griddata((glon,glat),gz,(LONG,LAT),method='linear')) 
            else:
                Z=np.ones(np.shape(LAT))*sourceGeometry                                   
            #### lonr,latr,rr are read from the file that contains spatial rate of magnitudes and rr is the density rate          
            #### density rate of magnitude mi at the grid point
            R=np.matrix(griddata((lonr,latr),rr,(LONG,LAT),method='linear'))  

            #### The exceedance rate of strong ground motion because of magnitude mi for all asked acceleration values
            exceedance_rate_m=np.arange(len(inputsH.Accelerationlist))*0
            
            #### To count grid points on the subduction interface
            zvalue=0 
            #### To count grid points with density rate value for magnitude mi
            rvalue=0              
            #### To count the grid points
            k=0
            
            for i in range(len(LONG)): 
                k=k+1
                    
                #### If the grid point represents a point with depth value  
                if not math.isnan(Z[i,0]):       
                    zvalue=zvalue+1
                    r=geopy.distance.distance((LAT[i,0],LONG[i,0]),(cglat,cglon)).km
                        
                    #### If the grid point represents a point with density rate for magnitue mi
                    if not math.isnan(R[i]):
                        rvalue=rvalue+1

                        #### landa_m_g is the rate at grid point (in unit of time and over area increment)
                        landa_m_g=R[i,0]*DA[i,0]
                                                         
                            
                        #### The exccedance rate of strong ground motion because of magnitude mi at grid point g for all asked acceleration values
                        exceedance_rate_m_g=[]   
                            
                        for ai in inputsH.Accelerationlist:
                            #### prob_exceedance_a=P(a > ai)                             
                            prob_exceedance_a=PexceedaFunction(ai,mi,r,round(Z[i,0],3))

                            #### exceedance_rate_m_g=[exceedance_rate_m_g(a>a1), exceedance_rate_m_g(a>a2),....]               
                            exceedance_rate_m_g.append(prob_exceedance_a*landa_m_g) 

                           
                        #### exceedance_rate_m=[exceedance_rate_m(a>a1),exceedance_rate(a>a2),....] 
                        exceedance_rate_m=np.add(exceedance_rate_m,exceedance_rate_m_g) 
            
            print('----%i of %i grid points have z value along source and %i have rate.'%(zvalue,k,rvalue))   
            print('----The exceedance rate because of magnitude %.1f over all %i grid points around the calculation site is estimated for all asked acceleration values'%(mi,rvalue))                         
            #### exceedance_rate=[exceedance_rate(a>a1),exceedance_rate(a>a2)....] 
            exceedance_rate=np.add(exceedance_rate,exceedance_rate_m)                                     
        print('--The total exceedance rate for all asked acceleration is estimated at the calculation site')   
        exceedance_rate_values=' '.join(str(float("{0:.2E}".format(ris))) for ris in exceedance_rate)        
        outputfile.writelines('%7.2f %6.2f %s\n'%(cglat,cglon,exceedance_rate_values))
    print('there are %i calculation points inLand'%inlandN)
    return 
#### =======================================================
#### =======================================================
#### =======================================================
def main():
    if inputsH.magnitude_dependent_depth_function:
        P_exc_function=P_exc_rate_depthDependent 
    else:
        P_exc_function=P_exc_rate    
    print ('***** Python version: ******\n'+sys.version+'\n--------------------\n')
    print ('***** This program uses the estimated Kernel rates and calculates  ')
    print ('***** BY POUYE.YAZDI@UPM.ES ******\n\nSTART\n--------------------') 
    time=datetime.datetime.now() 
    
    calcgridfile=open(os.path.join(g.inputsfilepath,inputsH.haz_calcgridfilename),'r')
    print ('(I) The calculation sites are given in %s.\n'%(os.path.join(g.inputsfilepath,inputsH.haz_calcgridfilename)))
    calcgrids=calcgridfile.readlines()
    calcgridfile.close()
    #------------------------------------------------------------------------------
    if P_exc_function==P_exc_rate:
        if not inputsH.constantDepth:
            P_exc_function=P_exc_rate_mexicansubduction
            print ('(II) The 3D grid points of seismic source are given in %s.\n'%(os.path.join(g.inputsfilepath,inputsH.sourcegeometryfilename)))
            sourcegeometryfile=open(os.path.join(g.inputsfilepath,inputsH.sourcegeometryfilename),'r')
            sourcegeometry=sourcegeometryfile.readlines()
            sourcegeometryfile.close()
        else:
            print ('(II) No 3D grid points of seismic source is given')
            sourcegeometry=inputsH.constantDepth 
            if inputsH.constantDepth<=5:
                print ('YOUR CONSTANT DEPTH IS 5 km or LESS!!!!!, IT MAY RESULT NO CALCULATION')
                sys.exit()
    elif P_exc_function==P_exc_rate_depthDependent:
        sourcegeometry=np.isnan
    #------------------------------------------------------------------------------
    print ('(III) The calculation consideres earthquakes with magnitudes >= %.1f.\n'%inputsH.mMinHazard)
    #------------------------------------------------------------------------------
    print ('(IV) The result of this function is written in file %s.\n'%(os.path.join(inputsH.gere1yfilepath,inputsH.gere1yfilename)))
    gere1yfile=open(os.path.join(inputsH.gere1yfilepath,inputsH.gere1yfilename),'w')    
        
    GERE_HAZARD(calcgrids,sourcegeometry,inputsH.mMinHazard,gere1yfile,P_exc_function)
    gere1yfile.close()
    print('Elapsed time for GERE_HAZARD : %s'%(datetime.datetime.now()-time))
if __name__=='__main__':  
    main()
