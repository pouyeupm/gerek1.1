#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  6 11:19:47 2018
by Pouye Yazdi
pouye.yazdi@upm.es
"""
"""
This code uses the ratefile made by KERNEL_RATES function as the main input. 
Then it calculate the Ground motion Exceedance Rate (GER) at any calculation 
site.
For each magnitudes, it makes a different grid of nodes (for picking up the 
rate) around the calculation site ---> it uses hazardGrid function below.
The GER at each calculation site should be calculated using all 
nodes of the constructed grid.
The calculations site which are offshore will be discarded in the process.
It is important to have the depth of the grid points. If there is no depth
value for them they will be discarded in calculation of the GER.
"""
import os
import sys
import numpy as np
import datetime
import scipy
from scipy.stats import norm
from shapely.geometry import Point
import inputsH
import geopy.distance
import math
from scipy.interpolate import griddata
import g

#### =======================================================
#### =======================================================
#### =======================================================
def hazardGrid(m):
    if m<=4.5:
        x=[-25,-20,-15,-12,-9,-6,-3,-1,1,3,6,9,12,15,20,25]
        dx=[5,5,4,3,3,3,2.5,2,2,2.5,3,3,3,4,5,5]
    elif m<=5.0:
        x=[-40,-30,-25,-20,-15,-12,-9,-6,-3,-1,1,3,6,9,12,15,20,25,30,40]
        dx=[10,7.5,5,5,4,3,3,3,2.5,2,2,2.5,3,3,3,4,5,5,7.5,10]
    elif m<=5.5:
        x=[-50,-40,-35,-30,-25,-20,-15,-12,-9,-6,-3,-1,1,3,6,9,12,15,20,25,30,35,40,50]
        dx=[10,7.5,5,5,5,5,4,3,3,3,2.5,2,2,2.5,3,3,3,4,5,5,5,5,7.5,10]
    elif m<=6.0:
        x=[-80,-70,-60,-50,-40,-35,-30,-25,-20,-15,-12,-9,-6,-3,-1,1,3,6,9,12,15,20,25,30,35,40,50,60,70,80]
        dx=[10,10,10,10,7.5,5,5,5,5,4,3,3,3,2.5,2,2,2.5,3,3,3,4,5,5,5,5,7.5,10,10,10,10]
    elif m<6.5:
        x=[-100,-80,-65,-55,-45,-40,-35,-30,-25,-20,-15,-12,-9,-6,-3,-1,1,3,6,9,12,15,20,25,30,35,40,45,55,65,80,100]
        dx=[20,17.5,12.5,10,7.5,5,5,5,5,5,4,3,3,3,2.5,2,2,2.5,3,3,3,4,5,5,5,5,5,7.5,10,12.5,17.5,20]
    else:
        x=[-200,-150,-100,-80,-65,-55,-45,-40,-35,-30,-25,-20,-15,-12,-9,-6,-3,-1,1,3,6,9,12,15,20,25,30,35,40,45,55,65,80,100,150,200]
        dx=[50,50,35,17.5,12.5,10,7.5,5,5,5,5,5,4,3,3,3,2.5,2,2,2.5,3,3,3,4,5,5,5,5,5,7.5,10,12.5,17.5,35,50,50]
    return dx,x
#### =======================================================
#### =======================================================
#### =======================================================
def Rates(minM):
    r=open(os.path.join(inputsH.appliedratefilepath,inputsH.appliedratefilename),'r')    
    l=r.readlines()
    r.close()
    header=l[0]
    footer=l[-1]
    minKr=round(float(header.split()[2]),1)
    maxKr=round(float(header.split()[-1]),1)
    
    l=l[1:-1]
    xkm=[]
    ykm=[]
    for i in range(len(l)):
        x,y=g.P(float(l[i].split()[1]),float(l[i].split()[0]))
        xkm.append(x/1000)
        ykm.append(y/1000)
        
    if minM<(minKr-0.01):
        print('The given min magnitude for GERE is lower than minimum magnitude for the Kernel rates\n It is now set to %.1f'%(round(float(header.split()[2],1))))
        minM=minKr
    magRange=np.arange(minM,maxKr+0.01,0.1) 
 
    density_rates=[]
    for mag in magRange:
        n=int((mag-minKr)*10)
        magrate=[]      
        for i in range(len(l)):
            magrate.append(round(float(l[i].split()[2+n]),10))
        density_rates.append(np.array(magrate)/round(float(footer[14:-1]),2))
    return xkm,ykm,magRange,density_rates
#### =======================================================
#### =======================================================
#### =======================================================
def P_exc_rate(a,mag,r,z): 
    #this function gets the entered as z! that can be a constant value inputsH.constantDepth or can be the result of reading the 3Dsource geometry file
    prob=0
    if z>5:
        zdistribution=np.array([-5,0,5,15])
        wi=np.array([0.25,0.35,0.25,0.15])
        zrange=zdistribution+z
        for i in range(4):
            ln_a,Sigma_lna=Att(mag,r,zrange[i])
            Z=(ln_a-np.log(a))/Sigma_lna
            prob=prob+(norm.cdf(Z)*wi[i])                 
    return prob 
#### =======================================================
#### =======================================================
#### =======================================================
def P_exc_rate_depthDependent(a,mag,r,z):
    #this function does not care what enters as z! the z is defined here
    prob=0
    if mag<5:
        z=10
    elif mag<6:
        z=15
    else:
        z=20
    if z>5:
        zdistribution=np.array([-5,0,5,15])
        wi=np.array([0.25,0.35,0.25,0.15])
        zrange=zdistribution+z
        for i in range(4):
            ln_a,Sigma_lna=Att(mag,r,zrange[i])
            Z=(ln_a-np.log(a))/Sigma_lna
            prob=prob+(norm.cdf(Z)*wi[i])                 
    return prob
#### =======================================================
#### =======================================================
#### =======================================================
#### GMPE by Arroyo et al. 2010
def Att(mag,r,z):
    R=np.sqrt(r*r+z*z)
    r02=1.4447E-5 *np.exp(2.3026*mag)                                   
    C=0.0150*R
    D=0.0150*np.sqrt(R*R+r02)
    CC=-1*scipy.special.expi(-C)
    DD=-1*scipy.special.expi(-D)
    A=(CC-DD)/(r02)
    lna=2.4862+(0.9392*mag)+(0.5061*np.log(A))
    sigmalna=0.75   
    return lna,sigmalna   
#### =======================================================
#### =======================================================
#### =======================================================

def GERE_HAZARD(calculationGrdid,sourceGeometry,minHazardMag,outputfile,PexceedaFunction):
    print('(V) GERE_HAZARD is going to be executed for\n %i asked strong ground motion values: %s'%(len(inputsH.Accelerationlist),inputsH.Accelerationlist))
    print('(VI) The magnitude density rates are picked up from the result of KERNEL_RATES function.')
    #### rates are in 1Km2
    xr,yr,magrange,rates=Rates(minHazardMag)

    gx=[]
    gy=[]
    gz=[]
    if type(sourceGeometry)==list:
        print('(VII) You have introduced a 3D geometry that is used for calculation of source depth.')
        for grid in sourceGeometry:
            gridlon,gridlat=float(grid.split()[0]),float(grid.split()[1])
            gridx,gridy=np.array(g.P(gridlon,gridlat))/1000
            gridz=round(float(grid.split()[2]),2)
            gx.append(gridx) # in km
            gy.append(gridy) # in km
            gz.append(gridz) # in km and positive
    else:
        print('(VII) You have introduced a constant depth of %.f km for your source.'%sourceGeometry)
             
    outputfile.writelines('lat long %s\n'%(' '.join('%s'%a for a in inputsH.Accelerationlist)))
    N=0
    inlandN=0
    for cg in calculationGrdid:
        N=N+1
        #### =======================================================
        #### Converting lat long coordinates of the site to km 
        cglon,cglat=float(cg.split()[0]),float(cg.split()[1]) 
        cgx,cgy=np.array(g.P(cglon,cglat))/1000 
        print('\n********\n--%i-For calculation site %.2f , %.2f'%(N,cglon,cglat))    
        #### =======================================================
        #### The exceedance rate of strong ground motion because for all asked acceleration values
        exceedance_rate=np.arange(len(inputsH.Accelerationlist))*0         
        if not inputsH.Landpoly.contains(Point(cglon,cglat)):
            print('*--This calculation point is outside land and will be skipped')
        else:
            inlandN=inlandN+1  
            for h in range(len(magrange)):
                mi=magrange[h]
                rr=rates[h]
                print('----Magnitude: %.1f'%mi) 
                dx,xe=hazardGrid(mi)
                dy=dx
                print('----A grid of size %ikm x %ikm is made'%(max(xe),max(xe)))
                
                #### gxi and gyi are in km
                gxi=np.array(xe)+cgx 
                gyi=np.array(xe)+cgy 
                X,Y=np.meshgrid(gxi,gyi)
                DX,DY=np.meshgrid(dx,dy)
                DA=np.matrix(DX*DY) 
                
                #### For converting to Lat Long we need coordinates in meter
                aLONG,aLAT=g.P(X*1000,Y*1000,inverse=True) 
                LONG,LAT=np.matrix(aLONG),np.matrix(aLAT)
                
                #### gx,gy,gz are the read from the file that contains source geometry coordinates
                #### depth of the grid point is:
                if not len(gz)==0:
                    Z=np.matrix(griddata((gx,gy),gz,(X,Y),method='linear')) 
                else:
                    Z=np.ones(np.shape(X))*sourceGeometry                                   
                #### xr,xy,rr are read from the file that contains spatial rate of magnitudes and rr is the density rate          
                #### density rate of magnitude mi at the grid point
                R=np.matrix(griddata((xr,yr),rr,(X,Y),method='linear'))  
    
                #### The exceedance rate of strong ground motion because of magnitude mi for all asked acceleration values
                exceedance_rate_m=np.arange(len(inputsH.Accelerationlist))*0
                
                #### To count grid points on the subduction interface
                zvalue=0 
                #### To count grid points with density rate value for magnitude mi
                rvalue=0              
                #### To count the grid points
                k=0
                
                for j in range(len(gyi)): 
                    for i in range(len(gxi)):
                        k=k+1
                        
                        #### If the grid point represents a point with depth value  
                        if not math.isnan(Z[j,i]):       
                            zvalue=zvalue+1
                            r=geopy.distance.distance((round(LAT[j,i],3),round(LONG[j,i],3)),(cglat,cglon)).km
                            
                            #### If the grid point represents a point with density rate for magnitue mi
                            if not math.isnan(R[j,i]):
                                rvalue=rvalue+1
    
                                #### landa_m_g is the rate at grid point (in unit of time and over area increment)
                                landa_m_g=R[j,i]*DA[j,i]
                                                             
                                
                                #### The exccedance rate of strong ground motion because of magnitude mi at grid point g for all asked acceleration values
                                exceedance_rate_m_g=[]   
                                
                                for ai in inputsH.Accelerationlist:
                                    #### prob_exceedance_a=P(a > ai)                             
                                    prob_exceedance_a=PexceedaFunction(ai,mi,r,round(Z[j,i],3))
    
                                    #### exceedance_rate_m_g=[exceedance_rate_m_g(a>a1), exceedance_rate_m_g(a>a2),....]               
                                    exceedance_rate_m_g.append(prob_exceedance_a*landa_m_g) 
    
                               
                                #### exceedance_rate_m=[exceedance_rate_m(a>a1),exceedance_rate(a>a2),....] 
                                exceedance_rate_m=np.add(exceedance_rate_m,exceedance_rate_m_g) 
                
                print('----%i of %i grid points have z value along source and %i have rate.'%(zvalue,k,rvalue))   
                print('----The exceedance rate because of magnitude %.1f over all %i grid points around the calculation site is estimated for all asked accelaration values'%(mi,rvalue))                         
                #### exceedance_rate=[exceedance_rate(a>a1),exceedance_rate(a>a2)....] 
                exceedance_rate=np.add(exceedance_rate,exceedance_rate_m)                                     
            print('--The total exceedance rate for all aske acceleration is estimated at the calculation site')   
            exceedance_rate_values=' '.join(str(float("{0:.2E}".format(ris))) for ris in exceedance_rate)        
            outputfile.writelines('%7.2f %6.2f %s\n'%(cglat,cglon,exceedance_rate_values))
    print('there are %i calculation points inLand'%inlandN)
    return 
#### =======================================================
#### =======================================================
#### =======================================================
def main():
    P_exc_function=P_exc_rate
#    P_exc_function=P_exc_rate_depthDependent    
    print ('***** Python version: ******\n'+sys.version+'\n--------------------\n')
    print ('***** This program uses the estimated Kernel rates and calculates  ')
    print ('***** BY POUYE.YAZDI@UPM.ES ******\n\nSTART\n--------------------') 
    time=datetime.datetime.now() 
    
    calcgridfile=open(os.path.join(g.inputsfilepath,inputsH.haz_calcgridfilename),'r')
    print ('(I) The calculation sites are given in %s.\n'%(os.path.join(g.inputsfilepath,inputsH.haz_calcgridfilename)))
    calcgrids=calcgridfile.readlines()
    calcgridfile.close()
    #------------------------------------------------------------------------------
    if not inputsH.constantDepth:
        print ('(II) The 3D grid points of seismic source are given in %s.\n'%(os.path.join(g.inputsfilepath,inputsH.sourcegeometryfilename)))
        sourcegeometryfile=open(os.path.join(g.inputsfilepath,inputsH.sourcegeometryfilename),'r')
        sourcegeometry=sourcegeometryfile.readlines()
        sourcegeometryfile.close()
    else:
        print ('(II) No 3D grid points of seismic source is given')
        sourcegeometry=inputsH.constantDepth            
    #------------------------------------------------------------------------------
    print ('(III) The calculation consideres earthquakes with magnitudes >= %.1f.\n'%inputsH.mMinHazard)
    #------------------------------------------------------------------------------
    print ('(IV) The result of this function is written in file %s.\n'%(os.path.join(inputsH.gere1yfilepath,inputsH.gere1yfilename)))
    gere1yfile=open(os.path.join(inputsH.gere1yfilepath,inputsH.gere1yfilename),'w')    
        
    GERE_HAZARD(calcgrids,sourcegeometry,inputsH.mMinHazard,gere1yfile,P_exc_function)
    gere1yfile.close()
    print('Elapsed time for GERE_HAZARD : %s'%(datetime.datetime.now()-time))
if __name__=='__main__':  
    main()