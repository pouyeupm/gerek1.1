#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec  3 20:22:16 2019

@author: pouye
"""

import os
catfilename='Catalogo04122012.txt'
inputsfilepath=os.path.abspath('')

file=open(os.path.join(inputsfilepath,'corrected-%s'%catfilename),'w')
file.write('year month day lat long epiError(km) magnitude magError cYear directivityW directivityAz depth(+km)\n')
catfile=open(os.path.join(inputsfilepath,catfilename),'r')
catalog=catfile.readlines()[1:]
catfile.close()
for cat in catalog:
    year=int(cat.split(';')[0])
    month=int(cat.split(';')[1])
    day=int(cat.split(';')[2])
    lat=round(float(cat.split(';')[4]),4)
    lon=round(float(cat.split(';')[3]),4)
    depth=round(float(cat.split(';')[5]),1)
    mag=round(float(cat.split(';')[6]),1)
    dmag=round(float(cat.split(';')[7]),1)
    if year<1900:depi=10
    elif year<1963:depi=5
    elif year<1980:depi=2
    elif year>1979:depi=1
    w_az=0
    az=0
    if mag<3.5:cYear=1978
    elif mag<4.0:cYear=1975
    elif mag<4.5:cYear=1908
    elif mag<5.0:cYear=1883
    elif mag<5.5:cYear=1800
    elif mag<6.0:cYear=1520
    elif mag>5.99:cYear=1048

    file.write('%i\t%i\t%i\t%.4f\t%.4f\t%.1f\t%.1f\t%.1f\t%.2f\t%.1f\t%.1f\t%s\n'%(year,month,day,lat,lon,depi,mag,dmag,cYear,w_az,az,depth))
file.close()


catfilename='mix1900-2018.txt'
inputsfilepath=os.path.abspath('')

file=open(os.path.join(inputsfilepath,'corrected-%s'%catfilename),'w')
file.write('year month day lat long epiError(km) magnitude magError cYear directivityW directivityAz depth(+km)\n')
catfile=open(os.path.join(inputsfilepath,catfilename),'r')
catalog=catfile.readlines()[1:]
catfile.close()
for event in catalog:
    year=int(event[0:4])
    month=int(event[5:7])
    day=int(event[8:10])
    lat=round(float(event[13:20]),4)
    lon=round(float(event[20:28]),4)
 
    depi=round(float(event[32:35]),1)
    depth='nun'
    mag=round(float(event[37:40]),1)
    dmag=round(float(event[41:45]),1)
    cYear=round(float(event[46:53]),2)
    w_az=round(float(event[56:59]),1)
    az=round(float(event[61:66]),1)

    file.write('%i\t%i\t%i\t%.4f\t%.4f\t%.1f\t%.1f\t%.1f\t%.2f\t%.1f\t%.1f\t%s\n'%(year,month,day,lat,lon,depi,mag,dmag,cYear,w_az,az,depth))
file.close()