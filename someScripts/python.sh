#!/usr/bin/env python

#The file with the Python code must be located in your current working directory. The file must be in the Python Module Search Path (PMSP), where Python looks for the modules and packages you import.To know what’s in your current PMSP, you can run the following code:

import sys
for path in sys.path:
    print(path)
    
