#!/bin/bash
path=/usr/bin/python3

<<EK_BLOCK
To estimate the kernel rate, set the inputs inside inputs.py and 
activate the scripts of this block 
EK_BLOCK
#chmod +x Krate_generator.py
#$path Krate_generator.py 

<<GER_BLOCK
To estimate the exceedance rate of the ground motion in one year, set the inputs inside inputsH.py and activate the scripts of this block.
GER_BLOCK
chmod +x GERE1y_generator.py 
$path GERE1y_generator.py 

