#!/bin/bash
path=/usr/bin/python3

#Kratefilename='Krates_all_min4.0_max8.4_Q2.0_G0.50_D0.700_Em1_EEI3_dA472.2km2_Guerrero1900-2018.txt'
<<RATE_PLOT_BLOCK
To estimate the kernel rate, set the inputs inside inputs.py and 
activate the scripts of this block 
RATE_PLOT_BLOCK
#rate_plotter_title='Testing plot'
#minMRP=5.0
#maxMRP=5.5
#chmod +x Krate_plotter.py
#$path ./Krate_plotter.py $Kratefilename $minMRP $maxMRP $rate_plotter_title


GERfilename='GERE1yMmin5.0_HazardGrid03x03_Lat17.0_18.0Lon-102.0_-100.0_Krates_all_min4.0_max8.4_Q2.0_G0.50_D0.700_Em1_EEI3_dA472.2km2_Guerrero1900-2018.txt'
<<GM_PLOT_BLOCK
To estimate the kernel rate, set the inputs inside inputs.py and 
activate the scripts of this block 
GM_PLOT_BLOCK
TimeWindow=50
Prob_levels='0.1 0.05 0.01 0.005'
chmod +x GER_plotter.py
$path ./GER_plotter.py $GERfilename $TimeWindow $Prob_levels


<<HAZARD_CURVE_PLOT_BLOCK
To estimate the kernel rate, set the inputs inside inputs.py and 
activate the scripts of this block 
HAZARD_CURVE_PLOT_BLOCK
#TimeWindow=50
#lat=17.5
#long=-101
#Prob_levels='0.02 0.05 0.001'
#chmod +x Hcurve_plotter.py
#$path ./Hcurve_plotter.py $GERfilename $TimeWindow $lat $long $Prob_levels