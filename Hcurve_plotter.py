#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec  4 20:29:58 2019
by Pouye Yazdi
pouye.yazdi@upm.es
"""

import os
import sys
import numpy as np
import matplotlib.pyplot as plt 
from scipy.interpolate import griddata
from scipy.interpolate import interp1d
import g


def PlotHazardCurve(plot_filename,plot_filepath,Nyears,sitelat,sitelong,probabilityList):
    pointZ=(sitelong,sitelat)
    
    
    r=open(os.path.join(plot_filepath,plot_filename),'r')    
    l=r.readlines()
    r.close()
    header=l[0]
    acclist=[round(float(a),1) for a in header.split()[2:]]
    accMin=acclist[0]
    accMax=acclist[-1]
    print('The given Ground motion Exceedance Rate file is estimated for %i accelerations between %.1f and %.1f, both included'%(len(acclist),accMin,accMax))            
    l=l[1:]
    
    
    lonLIST=[]
    latLIST=[]
    exr1yLIST=[]
    pLIST=[]
    aLIST=[]
    for i in range(len(l)):
        lat=float(l[i].split()[0])
        lon=float(l[i].split()[1])
        exr1ylist=[round(float(a),11) for a in l[i].split()[2:]]
        exr1yArray=np.array(exr1ylist)
        pArray=1-np.exp(-1*exr1yArray*Nyears)
        plist=list(pArray)
        try:
            interpolation_pArray=interp1d(np.log10(plist),np.log10(acclist), kind='linear')
        except:
            print('--interpolation is not possible for point at %.2f,%.2f'%(lat,lon))
        lonLIST.append(lon)
        latLIST.append(lat)
        exr1yLIST.append(exr1ylist)
        pLIST.append(plist) 
        
        alist=[]
        for p in probabilityList:
            try:
                log10accel=interpolation_pArray(np.log10(p))
            except:
                log10accel=0
            if log10accel==0:
                alist=alist+[np.NaN]
            else:
                alist=alist+[round(float(np.power(10,log10accel)),2)]
        aLIST.append(alist)
        
    
    pLIST=np.reshape(pLIST,(len(l),len(acclist)))     
    aLIST=np.reshape(aLIST,(len(l),len(probabilityList)))  
    
    probZ=[]
    for i in range(len(acclist)):
        probZ=probZ+[griddata((lonLIST,latLIST),pLIST[:,i],pointZ,method='linear')]
        
    accelZ=[]
    for j in range(len(probabilityList)):
        accelZ=accelZ+[griddata((lonLIST,latLIST),aLIST[:,j],pointZ,method='linear')]
    

        
    fig= plt.figure(1)
    ax = fig.add_subplot(1,1,1)
    ax.plot(acclist,probZ)
    ax.plot(accelZ,probabilityList,'*',color='k')
    ax.set_xscale('log')
    ax.set_yscale('log')
    ax.grid(b=True, which='both', color='0.65', linestyle='-',linewidth=0.5)
    for tick in ax.get_xticklabels():
        tick.set_fontsize(10)
    for tick in ax.get_yticklabels():
        tick.set_fontsize(10)
    ax.tick_params(axis="y",which='both',direction="out", pad=0)
    ax.tick_params(axis="x",direction="out",pad=0)
    ax.xaxis.labelpad = -5
    ax.set_xlabel('PGA (gal)',family='Helvetica',fontsize=10)
    ax.set_ylabel('Exceedance Probability in %i years'%Nyears,family='Helvetica',fontsize=10)
    plt.savefig(os.path.join(plot_filepath,'HazardCurve_at_%.3f_%.3f_in%iYears.png'%(sitelong,sitelat,Nyears)), dpi=300, facecolor='w', edgecolor='w',
                orientation='portrait',format='png')
    plt.show()

#### =======================================================
#### =======================================================
#### =======================================================
def main(argv): 
    print ('***** This code is for plotting the hazard curve at given site.')
    PlotHazardCurve(argv[0],g.hazfilepath,int(argv[1]),float(argv[2]),float(argv[3]),[float(a) for a in argv[4:]])

if __name__=='__main__':  
    ins=sys.argv[1:]
    if len(ins)<5:
        print('ERROR! Not enough inputs\n Please enter inputes with space between them.\n Example:\n GER1y-filename 1 100 400 1100')
        sys.exit(1)
    main(ins)