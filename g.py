#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  2 15:34:04 2019
by Pouye Yazdi
pouye.yazdi@upm.es
"""

import pyproj
import numpy as np
import os

# =======================================================
# +++++  Some aspects of the study area  ++++++++++++++++
# =======================================================
UTMZONE=14 
aproxlong=-102 # an approximation of mean of your long range in ratecalcgridfilename
aproxlat=17 # an approximation of mean of your lat range in ratecalcgridfilename
# =======================================================
# +++ The directory for the input files and grid files ++
# =======================================================
inputsfilepath=os.path.abspath('inputs')
ratefilepath=os.path.abspath('outputs/KernelRates')
hazfilepath=os.path.abspath('outputs/HazardCalc')
#
#
#
#### End of inputs for primary settings
#### Do not make change in the following lines
#### Unless you want to :-D
#
#
#
#
#
#
#
#
#
#
#
#
#
#
# =======================================================
# ++++++++ The function that makes the grid files +++++++
# =======================================================
P=pyproj.Proj(proj='utm',zone=UTMZONE,ellps='WGS84')
def makegridfile(minlon,maxlon,minlat,maxlat,inclong,inclat,gridfilename):
    longarray=np.arange(minlon,maxlon+inclong,inclong)
    latarray=np.arange(minlat,maxlat+inclat,inclat)
    gridfile=open(os.path.join(os.path.abspath(inputsfilepath),gridfilename),'w')
    for long in longarray:
        for lat in latarray:
            gridfile.write('%.3f %.3f\n'%(long,lat))
    return


